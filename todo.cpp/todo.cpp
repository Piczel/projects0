#include <iostream>
#include <fstream>

#include <string>

using namespace std;

void
showAll(ofstream);

void
printFirst(ofstream);

void
addLast(ofstream, string const&);

void
eraseFirst(ofstream);

int
main(int argc, char *argv[]){
	string const todoList("$USER/.todo");
	ofstream list(todoList.c_str(),ios::app);

	if(list){
		string arg(argv[1]);
	
		// regarder les arguments
		if(argc == 1){
			printFirst(list);
		} else if(arg.compare("pop") == 0){
			eraseFirst(list);
		} else if(arg.compare("push") == 0){
			string instruction(argv[2]);
			addLast(list,instruction);
		} else if(arg.compare("list") == 0){
			showAll(list);
		}	else {
			cout << "There is a problem in the arguments, see the README" << endl;
		}
	} else {
		cout << "There is a problem with the todo-file" << endl;
	}

	return 0;
}

void
showAll(ofstream list){
	int line(0);
	string currentLine("");

	while(getline(list,currentLine)){
		line++;
		cout << line << ". " << currentLine << endl;
	}
}

void
printFirst(ofstream list){
	string currentLine("");
	getline(list,currentLine);

	cout << currentLine << endl;
}

void
addLast(ofstream list, string const& instruction){
	list.seekp(0,ios::end);
	list << instruction << endl;
	cout << "Current list :" << endl;
	showAll(list);
}

void
eraseFirst(ofstream list){
	string line("");
	cout << getline(list,line) << endl;
	
}

