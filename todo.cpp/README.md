# todo

This is a C++ program which simulates a todo-list.

# Usage :

./todo

--> to see what's the last item

./todo push "a new instruction"

--> to add new item to your todo-list

./todo pop

--> to remove the last item of your todo-list

./todo list

--> to show all the items of your todo-list
