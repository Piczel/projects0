#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"


extern char buffWord[255];
extern char buffLine[255];


void
readIntoFile_word(FILE *fileReaded, char *wordSearched){
	unsigned int i = 0;
	unsigned int j = 0;
	char character;
	int result = 1;

	while((character = getc(fileReaded)) != EOF){
		if(character == ' ' || character == '\n'){
			if(strcmp(buffWord,wordSearched) == 0){
				result = 0;
			}
			cleanWordBuffer();
			if(character == '\n'){
				if(result == 0)
					showBuffer();
				cleanLineBuffer();
				result = 1;
				j = 0;
			} else {
				puttInBufferLine(character, &j);
			}
			i = 0;
		} else {
			puttInBuffer(character, &i, &j);
		}
	}
}

