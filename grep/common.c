#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char buffLine[255];
char buffWord[255];


int
contains(char *word, char character){
	for(int i = 0; word[i] != '\0'; i++)
		if(word[i] == character)
			return 0;

	return -1;
}


void
cleanLineBuffer(){
	buffLine[0] = '\0';
}


void
cleanWordBuffer(){
	buffWord[0] = '\0';
}


void
showBuffer(){
	printf("%s\n",buffLine);
}


void
puttInBuffer(char character, unsigned int *i, unsigned int *j){
	buffLine[*j] = character;
	buffWord[*i] = character;
	(*i)++;
	(*j)++;
	buffLine[*j] = '\0';
	buffWord[*i] = '\0';
}


void
puttInBufferLine(char character, unsigned int *i){
	buffLine[*i] = character;
	(*i)++;
	buffLine[*i] = '\0';
}
