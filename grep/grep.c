#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "checkWithWord.h"
#include "common.h"
#include "checkIntoFile.h"

#include <unistd.h>
extern char *optarg;

int
main(int argc, char **argv){

	FILE *fileToRead;
	fileToRead = (argc == 3) ? fopen(argv[2],"r") : fopen(argv[3],"r");


	if(argc < 3 || argc > 10){
	 printf("cat README.md pls");
	 return 1;
	}


	char opt;

	while((opt = getopt(argc,argv,"w:f:")) != -1){
		switch(opt){
		case 'w':
			readIntoFile_word(fileToRead, optarg);
			break;
		case 'f':
				{
					FILE *compare = fopen(optarg,"r");
					readIntoFile_file(fileToRead, compare);
					break;
		 		}
		default:
			printf("opt:%d, argc:%d\n", opt, argc);
			printf("u mad bro\n");
			break;
		}
	}


	fclose(fileToRead);

	return 1;
}

